#[derive(Clone)]
enum Field {
    Empty(Option<i32>),
    PathStart,
    PathEnd,
    Path,
    Wall,
}

impl std::fmt::Display for Field {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", match self {
            Field::Empty(val) => match val {
                Some(x) => format!("{:3}", x),
                None => String::from(" . ")},
            Field::Wall => String::from(" w "),
            Field::PathStart => String::from(" s "),
            Field::PathEnd => String::from(" e "),
            Field::Path => String::from(" x "),
        })
    }
}

#[derive(Clone, PartialEq, PartialOrd)]
struct Coordinate {
    x: usize,
    y: usize,
}

impl std::fmt::Display for Coordinate {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

struct Grid {
    grid: Vec<Vec<Field>>,
    width: usize,
    height: usize
}

impl std::fmt::Display for Grid {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}",
            self.grid.iter().map(
                |row| row.iter().map(|x| format!("{}", x))
                .collect::<Vec<String>>().join("  "))
            .collect::<Vec<String>>().join("\n")
        )
    }
}

impl Grid {
    fn new(width: usize, height: usize) -> Grid {
        Grid{
            grid: vec![vec![Field::Empty(None); width]; height],
            width,
            height
        }
    }

    fn get(&self, coord: &Coordinate) -> Option<&Field> {
        match self.grid.get(coord.y) {
            Some(row) => match row.get(coord.x) {
                Some(field) => Some(field),
                None => None,
            },
            None => None,
        }
    }

    fn get_neighbors(&self, coord: &Coordinate) -> Vec<Coordinate> {
        let mut nn = Vec::new();
        if coord.x < self.width - 1 {
            nn.push(Coordinate{x: coord.x + 1, y: coord.y});
        }
        if coord.x > 0 {
            nn.push(Coordinate{x: coord.x - 1, y: coord.y});
        }
        if coord.y < self.height - 1 {
            nn.push(Coordinate{x: coord.x, y: coord.y + 1});
        }
        if coord.y > 0 {
            nn.push(Coordinate{x: coord.x, y: coord.y - 1});
        }

        nn
    }

    fn get_empty_neighbors(&self, coord: &Coordinate) -> Vec<Coordinate> {
        self.get_neighbors(coord).into_iter()
            .filter(|x| match self.get(x) {
                Some(Field::Empty(None)) => true,
                _ => false,
            })
            .collect::<Vec<Coordinate>>()
    }


    fn set(&mut self, coord: &Coordinate, value: i32) {
        match self.get(&coord) {
            Some(field) => self.grid[coord.y][coord.x] = Field::Empty(Some(value)),
            None => (),
        }
    }

    fn set_path(&mut self, path: &Vec<Coordinate>) {
        for coord in path {
            match self.get(coord) {
                Some(field) => self.grid[coord.y][coord.x] = Field::Path,
                None => (),
            }
        }
    }

    fn cost_from(&mut self, start: Coordinate) {
        self.set(&start, 0);
        let mut current = vec![start];
        let mut index = 0;

        while !current.is_empty() {
            index += 1;
            let mut next = Vec::new();
            for coord in current {
                let nn = self.get_empty_neighbors(&coord);
                nn.iter().for_each(|x| self.set(x, index));
                next.extend(nn.into_iter());
            }
            current = next;

            // current.iter().for_each(|x| self.set(x, index));
            // current = current.into_iter()
            //     .flat_map(|x| self.get_empty_neighbors(&x)).collect();
            // index += 1;
            for c in &current {print!("{}", c);}
            println!("");
            // if index > 3 {break}
        }
    }

    fn get_shortest_path(&mut self, start: Coordinate, end: Coordinate) -> Vec<Coordinate> {
        println!("hsortest path");
        self.cost_from(start);
        println!("{}", self);
        self.path_iter(end).collect::<Vec<Coordinate>>()
    }

    fn path_iter(&self, end: Coordinate) -> PathIterator {
        PathIterator{
            grid: self,
            current: end,
        }
    }

}

struct PathIterator<'g> {
    grid: &'g Grid,
    current: Coordinate,
}

impl<'g> Iterator for PathIterator<'g> {
    type Item = Coordinate;
    fn next(&mut self) -> Option<Self::Item> {
        println!("nexting");
        let neighbors = self.grid.get_neighbors(&self.current);
        println!("first neighbour {}", neighbors[0]);
        if neighbors.is_empty() {
            return None;
        }

        let (coord, min) = neighbors.into_iter()
            .filter_map(|coord| {
                println!("{}", coord);
                match self.grid.get(&coord) {
                Some(Field::Empty(Some(value))) => Some((coord, value)),
                _ => None,

            }
            })
            .min_by(|(min_coord, min), (coord, value)| min.cmp(value))
            .unwrap();
        println!("at {} min {}", coord, min);
        self.current = coord.clone();
        if min.eq(&0) {return None;}
        Some(coord)
    }
}


fn main() {

    let start = Coordinate {x: 2, y: 2};
    let end = Coordinate {x: 7, y: 17};
    let mut grid = Grid::new(10, 20);

    let path = grid.get_shortest_path(start, end);

    grid.set_path(&path);
    
    // grid.cost_from(Coordinate{x:2, y:1});
    // let path = grid.get_empty_neighbors(&Coordinate{x:0, y:0});

    for p in path {
        println!("{}", p);
    }
    println!("{}", grid);
}
